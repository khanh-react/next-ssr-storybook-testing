import React from 'react';
import Button from './index';
import { ButtonProps } from './types';

export const Default = (props: ButtonProps) => {
  return <Button {...props} />;
};

export default {
  title: 'Components/Button',
  component: Button,
};
