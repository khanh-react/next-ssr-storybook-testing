import React from 'react';
import { Wrapper } from './styles';
import { ButtonProps } from './types';

export const Button = ({ style, size, text, type, isDisabled, isLoading, onPress }: ButtonProps) => {
  if (!isDisabled) {
    return (
      <Wrapper>
        <span>This is Button</span>
      </Wrapper>
    );
  }
};

export default Button;
