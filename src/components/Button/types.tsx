export type ButtonProps = {
  style?: any;
  text?: string;
  size?: string;
  type?: string;
  isDisabled?: boolean;
  isLoading?: boolean;
  onPress?: (e) => void;
};
