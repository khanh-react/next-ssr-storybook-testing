import React from 'react';
import Head from 'next/head';

function Header() {
  return (
    <Head>
      <title>BESTAFF RECRUITMENT</title>
      <meta name='viewport' content='initial-scale=1.0, width=device-width' />
    </Head>
  );
}

export default Header;
