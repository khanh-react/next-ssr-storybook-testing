import { types } from 'mobx-state-tree';

export const Todo = types.model({
  text: 'Learn Redux',
  completed: false,
  id: 0,
});

export const Todos = types
  .model({
    list: types.optional(types.array(Todo), []),
  })
  .views(self => ({
    get xxx() {
      return 123;
    },
  }))
  .actions(self => {
    const onGetTasks = () => {
      console.log('1');
    };
    return {
      onGetTasks,
    };
  });

export default Todos;
