import { useContext, createContext } from 'react';
import { types, Instance, onSnapshot } from 'mobx-state-tree';

import { Todos } from './todos';

const RootModel = types.model({
  todos: Todos,
});

let initialState = RootModel.create({
  todos: {},
});

const ISSERVER = typeof window === 'undefined';
if (!ISSERVER) {
  // Access localStorage
  const data = localStorage.getItem('rootState');
  if (data) {
    const json = JSON.parse(data);
    if (RootModel.is(json)) {
      initialState = RootModel.create(json);
    }
  }

  onSnapshot(initialState, snapshot => {
    console.log('Snapshot: ', snapshot);
    localStorage.setItem('rootState', JSON.stringify(snapshot));
  });
}

export const rootStore = initialState;

export type RootInstance = Instance<typeof RootModel>;
const RootStoreContext = createContext<null | RootInstance>(null);

export const Provider = RootStoreContext.Provider;

export function useMst() {
  const store = useContext(RootStoreContext);
  if (store === null) {
    throw new Error('Store cannot be null, please add a context provider');
  }
  return store;
}
