import React, { lazy, Suspense } from 'react';

const loadable = (importFunc, { fallback = null } = { fallback: null }) => {
  const LazyComponent = lazy(importFunc);
  const suspense = props =>
    <Suspense fallback={fallback}>
        <LazyComponent {...props} />
    </Suspense>;

  return (props: any) => suspense(props);
};

export default loadable;
