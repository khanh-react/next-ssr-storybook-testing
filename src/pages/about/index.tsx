import React from 'react';
import { observer } from 'mobx-react';
import { Button } from '../../components/Button';
import { useMst } from '../../models';

const AboutPage = observer(() => {
  const { todos } = useMst();
  return (
    <>
      <Button />
      <h1>Hello About Page 1111 {todos.xxx}</h1>
    </>
  );
});

export default AboutPage;
