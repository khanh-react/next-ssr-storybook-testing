import React from 'react';
import { Provider, rootStore } from '../models';
import '../styles/globals.css';

function App({ Component, pageProps }) {
  return (
    <Provider value={rootStore}>
      <Component />
    </Provider>
  );
}

export default App;
